package com.test.gambit.Player;
/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.gambit.Player.PlayerDataModel.PlayerData;
import com.test.gambit.Player.PlayerDataModel.Team;
import com.test.gambit.R;

import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerVieHolder> {

    private List<PlayerData> playerDataList;

    public PlayerAdapter(List<PlayerData> playerDataList) {
        this.playerDataList = playerDataList;
    }

    @NonNull
    @Override
    public PlayerAdapter.PlayerVieHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.player_item_view, viewGroup, false);
        return new PlayerAdapter.PlayerVieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerAdapter.PlayerVieHolder playerVieHolder, int i) {
        PlayerData playerData = playerDataList.get(i);
        Team team = playerData.getTeam();
        playerVieHolder.teamID.setText(team.getId().toString());
        playerVieHolder.playerFirstName.setText(playerData.getFirstName());
        playerVieHolder.playerLastName.setText(playerData.getLastName());
        playerVieHolder.name.setText(team.getName());
        playerVieHolder.position.setText("Position - " + playerData.getPosition() + "  ");
        playerVieHolder.fullName.setText(". " + team.getFullName() + "  ");
        playerVieHolder.conference.setText(" . " + team.getConference() + " / " + team.getDivision() + " ");
    }


    @Override
    public int getItemCount() {
        return playerDataList.size();
    }

    class PlayerVieHolder extends RecyclerView.ViewHolder {

        TextView playerFirstName;
        TextView playerLastName;
        TextView name;
        TextView teamID;
        TextView position;
        TextView fullName;
        TextView conference;

         PlayerVieHolder(@NonNull View itemView) {
            super(itemView);
            fullName = itemView.findViewById(R.id.full_name);
            playerFirstName = itemView.findViewById(R.id.first_name);
            playerLastName = itemView.findViewById(R.id.last_name);
            name = itemView.findViewById(R.id.name);
            teamID = itemView.findViewById(R.id.team_id);
            position = itemView.findViewById(R.id.position);
            conference = itemView.findViewById(R.id.conference);

        }
    }
}

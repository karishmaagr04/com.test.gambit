package com.test.gambit.Player;
/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.test.gambit.Core.AbstractFragment;
import com.test.gambit.Core.ViewModelEvent;
import com.test.gambit.DataApi;
import com.test.gambit.Player.PlayerDataModel.Player;
import com.test.gambit.R;
import com.test.gambit.RetrofitClientInstance;

import java.util.Objects;

public class PlayerFragment extends AbstractFragment<PlayerViewModel> {

    private RecyclerView recyclerView;
    private PlayerAdapter playerAdapter;
    private ProgressDialog progressDialog;
    private EditText editText;
    private ImageView imageView;
    private DataApi service;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_player, container, false);
        return root;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view_player);
        editText = view.findViewById(R.id.search_view);
        imageView = view.findViewById(R.id.image_view);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Fecthing players with name " + editText.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });
        getPlayerList();
    }

    private void getPlayerList() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        service = RetrofitClientInstance.getCacheEnabledRetrofit(Objects.requireNonNull(getActivity())).create(DataApi.class);
        viewModel.loadFirstDataCall(service);
    }

    @Override
    protected void registerEvents() {
        registerEvent("player_data");
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {

    }

    @Nullable
    @Override
    protected Class<PlayerViewModel> getViewModel() {
        return PlayerViewModel.class;
    }

    @Override
    protected void onDataChanged(ViewModelEvent viewModelEvent) {
        super.onDataChanged(viewModelEvent);
        if (viewModelEvent.eventType.equals("player_data")) {
            progressDialog.dismiss();
            final Player[] newList = new Player[1];
            newList[0] = (Player) viewModelEvent.eventData;
            playerAdapter = new PlayerAdapter(newList[0].getData());
            recyclerView.setAdapter(playerAdapter);

        }
    }
}

package com.test.gambit.Player.PlayerDataModel;
/*
 * @author Karishma Agrawal on 2019-05-23.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Player {

    @SerializedName("data")
    @Expose
    private List<PlayerData> data = null;
    @SerializedName("meta")
    @Expose
    private PlayerMeta meta;

    public List<PlayerData> getData() {
        return data;
    }

    public void setData(List<PlayerData> data) {
        this.data = data;
    }

    public PlayerMeta getMeta() {
        return meta;
    }

    public void setMeta(PlayerMeta meta) {
        this.meta = meta;
    }

}


package com.test.gambit.Player;
/*
 * @author Karishma Agrawal on 2019-05-30.
 */

import com.test.gambit.Core.AbstractViewModel;
import com.test.gambit.DataApi;

public class PlayerViewModel extends AbstractViewModel<PlayerModel> {
    @Override
    protected PlayerModel getModel() {
        return new PlayerModel();
    }

    public void loadFirstDataCall( DataApi service) {
        model.loadFirstDataCall(service);
    }
}
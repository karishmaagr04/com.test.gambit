package com.test.gambit.Player;
/*
 * @author Karishma Agrawal on 2019-05-30.
 */

import android.util.Log;

import com.test.gambit.Core.AbstractAsyncTask;
import com.test.gambit.Core.AbstractModel;
import com.test.gambit.Core.ViewModelEvent;
import com.test.gambit.DataApi;
import com.test.gambit.Player.PlayerDataModel.Player;
import com.test.gambit.RetrofitClientInstance;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class PlayerModel extends AbstractModel {

    public void loadFirstDataCall( DataApi service) {
        Task task = new Task(this, service);
        task.execute();
    }

    private static class Task extends AbstractAsyncTask<PlayerModel> {

        private DataApi service;

        Task(PlayerModel playerModel, DataApi service) {
            super(playerModel);
            this.service = service;
        }

        @Override
        protected ViewModelEvent onDoInBackground() {
            Call<Player> call = service.getPlayerData();
            Response<Player> execute = null;
            try {
                execute = call.execute();
                if (execute.isSuccessful()) {
                    return new ViewModelEvent(execute.body(), "player_data", ViewModelEvent.Status.SUCCESS);
                } else {
                    Log.d("netwrok_error", execute.message());
                    return new ViewModelEvent(null, "player_data", ViewModelEvent.Status.FAILURE);
                }

            } catch (IOException e) {
                Log.d("netwrok_error", e.getMessage());
                return null;
            }
        }

        @Override
        protected String getErrorMessage() {
            return null;
        }
    }


}

package com.test.gambit.Core;

/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.util.Log;

import java.util.Observable;

public abstract class AbstractModel extends Observable {

    protected final transient String TAG = this.getClass().getSimpleName();

    public void notifyObservers(ViewModelEvent viewModelEvent) {
        if (viewModelEvent != null) {
            Log.d("event error", viewModelEvent.eventType);
        }
        setChanged();
        super.notifyObservers(viewModelEvent);
    }

    /**
     * Override this method to do any clearing that is required before its ViewModel gets destroyed.
     * This method is auto called when associated ViewModel is getting destroyed.
     */
    protected void onCleared() {
    }
}

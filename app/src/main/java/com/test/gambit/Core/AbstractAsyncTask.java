package com.test.gambit.Core;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.ref.WeakReference;

public abstract class AbstractAsyncTask<T extends AbstractModel> extends AsyncTask<Void, Void, ViewModelEvent> {

    private final String TAG = this.getClass().getSimpleName();

    private final WeakReference<T> modelWeakReference;

    protected AbstractAsyncTask(T model) {
        this.modelWeakReference = new WeakReference<>(model);
    }

    protected T getModel() {
        return modelWeakReference.get();
    }

    @Override
    protected final ViewModelEvent doInBackground(Void... voids) {

        ViewModelEvent viewModelEvent;
        try {
            viewModelEvent = onDoInBackground();
        } catch (Exception e) {
            viewModelEvent = handleError(e);
        }
        if (viewModelEvent != null) {
            notifyObservers(viewModelEvent);
        }
        return viewModelEvent;
    }

    private ViewModelEvent handleError(Exception e) {
        try {
            return onError(e.getMessage(), e);
        } catch (Exception e1) {
            Log.d("event error", e1.getMessage(), e1);
            return null;
        }
    }

    private void notifyObservers(ViewModelEvent viewModelEvent) {
        AbstractModel baseModel = getModel();
        if (baseModel != null) {
            baseModel.notifyObservers(viewModelEvent);
        } else {
            Log.d(TAG, "data model is GCed, so no notification to data change observers.");
        }
    }

    /**
     * Override this if you need to do anything when there is a failure,
     * such as logging the error
     * <p>
     * Note: called from the context of the worker thread
     */
    protected ViewModelEvent onError(@Nullable String errorMessage, @Nullable Exception e) throws Exception {
        Log.d(TAG, errorMessage);
        return null;
    }

    /**
     * Override this if you need to do anything in non-ui thread
     *
     * <p>
     * Note: called from the context of the worker thread
     */
    protected abstract ViewModelEvent onDoInBackground() throws Exception;

    protected abstract String getErrorMessage();
}

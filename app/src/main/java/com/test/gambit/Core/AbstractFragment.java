package com.test.gambit.Core;

/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

public abstract class AbstractFragment<T extends AbstractViewModel> extends Fragment {

    protected T viewModel;
    private Class<T> aClass;

    @Override
    public final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            aClass = getViewModel();

            if (aClass != null) {
                registerEvent(AbstractViewModel.EVENT_SYSTEM_INTERNAL_ERROR);
            }
            registerEvents();
            doOnCreate(savedInstanceState);
        } catch (Exception e) {
            Log.d("ErrorInAbstractActivity", e.getLocalizedMessage());
        }
    }

    /**
     * Implement this to register events
     */
    protected abstract void registerEvents();

    /**
     * Implement this to do anything while the activity is getting created.
     */
    protected abstract void doOnCreate(@Nullable Bundle savedInstanceState);

    /**
     * Implement this to provide the class of your ViewModel. Return null if there isn't any.
     */
    protected abstract @Nullable
    Class<T> getViewModel();

    /**
     * Override this to do anything whenever there is change to the observable data communicated
     * in the form of a {@link ViewModelEvent}
     *
     * @param viewModelEvent The object which encapsulates the actual data and it's type.
     */
    protected void onDataChanged(ViewModelEvent viewModelEvent) {
    }

    protected final void registerEvent(String eventName) {
        if (aClass == null) {
            throw new IllegalStateException("ViewModel not provided in getViewModel()");
        }

        viewModel = ViewModelProviders.of(AbstractFragment.this).get(aClass);
        //noinspection unchecked
        viewModel.registerEvent(eventName).observe(this, new Observer<ViewModelEvent>() {
            @Override
            public void onChanged(@Nullable ViewModelEvent viewModelEvent) {
                try {

                    onDataChanged(viewModelEvent);
                } catch (Exception e) {
                    Log.d("event_error", e.getLocalizedMessage());
                }
            }
        });
    }
}
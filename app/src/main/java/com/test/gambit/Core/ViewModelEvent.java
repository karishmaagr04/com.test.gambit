package com.test.gambit.Core;
/*
 * @author Karishma Agrawal on 2019-05-29.
 */

public final class ViewModelEvent {

    /**
     * A unique string representing the type of event.
     */
    public final String eventType;

    public final Status status;

    //If status is FAILURE then eventData contains resource bundle id
    public final Object eventData;

    public ViewModelEvent(Object eventData, String eventType, Status status) {
        this.eventData = eventData;
        this.eventType = eventType;
        this.status = status;
    }

    public enum Status {
        LOADING, SUCCESS, FAILURE
    }

}

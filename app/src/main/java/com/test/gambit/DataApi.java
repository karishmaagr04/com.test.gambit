package com.test.gambit;
/*
 * @author Karishma Agrawal on 2019-05-23.
 */

import com.test.gambit.Game.GameDataModel.Game;
import com.test.gambit.Game.GameDataModel.GameData;
import com.test.gambit.Player.PlayerDataModel.Player;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DataApi {

    @GET("players")
    Call<Player> getPlayerData();

    @GET("games/{count}")
    Call<GameData> getGameDataCount(@Path("count") String count);

    @GET("games")
    Call<Game> getGameData();
}

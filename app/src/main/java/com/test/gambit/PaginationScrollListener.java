package com.test.gambit;
/*
 * @author Karishma Agrawal on 2019-05-29.
 */

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;

    public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (!isLastPage() && (getCurrentPage() - 1) % 25 == 0) {
            Log.d("pagination_scroll", getCurrentPage() + "");
            loadMoreItems();
        }
    }

    protected abstract void loadMoreItems();

    public abstract int getCurrentPage();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}

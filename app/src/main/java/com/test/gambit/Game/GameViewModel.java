package com.test.gambit.Game;
/*
 * @author Karishma Agrawal on 2019-05-30.
 */

import com.test.gambit.Core.AbstractViewModel;
import com.test.gambit.DataApi;

public class GameViewModel extends AbstractViewModel<GameModel> {
    @Override
    protected GameModel getModel() {
        return new GameModel();
    }

    public void loadFirstDataCall(int currentPage, DataApi service) {
        model.loadFirstDataCall(currentPage,service);
    }
}

package com.test.gambit.Game;
/*
 * @author Karishma Agrawal on 2019-05-29.
 */


import android.util.Log;

import com.test.gambit.Core.AbstractAsyncTask;
import com.test.gambit.Core.AbstractModel;
import com.test.gambit.Core.ViewModelEvent;
import com.test.gambit.DataApi;
import com.test.gambit.Game.GameDataModel.GameData;
import com.test.gambit.RetrofitClientInstance;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class GameModel extends AbstractModel {

    public void loadFirstDataCall(int currentPage,DataApi service) {
        Task task = new Task(this, currentPage, service);
        task.execute();
    }

    private static class Task extends AbstractAsyncTask<GameModel> {

        private DataApi service;
        private int currentPage;

        Task(GameModel gameModel, int currentPage, DataApi service) {
            super(gameModel);
            this.currentPage = currentPage;
            this.service = service;
        }

        @Override
        protected ViewModelEvent onDoInBackground() {
            Call<GameData> call = service.getGameDataCount(String.valueOf(currentPage));
            Response<GameData> execute = null;
            try {
                execute = call.execute();
                if (execute.isSuccessful()) {
                    return new ViewModelEvent(execute.body(), "abc", ViewModelEvent.Status.SUCCESS);
                } else {
                    return new ViewModelEvent(null, "abc", ViewModelEvent.Status.FAILURE);
                }

            } catch (IOException e) {
                Log.d("netwrok_error", e.getMessage());
                return null;
            }
        }

        @Override
        protected String getErrorMessage() {
            return null;
        }
    }


}

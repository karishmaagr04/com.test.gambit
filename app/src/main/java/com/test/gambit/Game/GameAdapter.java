package com.test.gambit.Game;
/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.gambit.Game.GameDataModel.GameData;
import com.test.gambit.R;

import java.util.List;

public class GameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GameData> gameDataList;
    private boolean isLoadingAdded = false;
    private int i = 1;

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    public GameAdapter(List<GameData> gameDataList) {
        this.gameDataList = gameDataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        GameData gameData = gameDataList.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                GameVieHolder gameVieHolder = (GameVieHolder) viewHolder;
                if (gameData != null && gameData.getHomeTeam() != null && gameData.getVisitorTeam() != null) {
                    gameVieHolder.homeTeamName.setText(gameData.getHomeTeam().getAbbreviation());
                    gameVieHolder.visitorTeamName.setText(gameData.getVisitorTeam().getAbbreviation());
                    gameVieHolder.homeTeamFullName.setText(gameData.getHomeTeam().getFullName());
                    gameVieHolder.visitorTeamFullName.setText(gameData.getVisitorTeam().getFullName());
                }
                break;
            case LOADING:
//                Do nothing
                break;
        }

    }


    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.game_item_view, parent, false);
        viewHolder = new GameVieHolder(v1);
        return viewHolder;
    }


    @Override
    public int getItemCount() {
        return gameDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == gameDataList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(GameData mc) {
        gameDataList.add(mc);
        Log.d("game_data_adapter", String.valueOf(i));
        i++;
        notifyItemInserted(gameDataList.size() - 1);
    }


    public void remove(GameData city) {
        int position = gameDataList.indexOf(city);
        if (position > -1) {
            gameDataList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
    }


    public GameData getItem(int position) {
        return gameDataList.get(position);
    }

    class GameVieHolder extends RecyclerView.ViewHolder {

        TextView homeTeamName;
        TextView visitorTeamName;
        TextView homeTeamFullName;
        TextView visitorTeamFullName;

        public GameVieHolder(@NonNull View itemView) {
            super(itemView);
            homeTeamName = itemView.findViewById(R.id.abbreviation1);
            homeTeamFullName = itemView.findViewById(R.id.full_name_game1);
            visitorTeamFullName = itemView.findViewById(R.id.full_name_game2);
            visitorTeamName = itemView.findViewById(R.id.abbreviation2);

        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }



}

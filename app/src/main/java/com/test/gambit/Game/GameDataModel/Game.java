package com.test.gambit.Game.GameDataModel;
/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Game {

    @SerializedName("data")
    @Expose
    private List<GameData> data = null;
    @SerializedName("meta")
    @Expose
    private GameMeta meta;

    public List<GameData> getData() {
        return data;
    }

    public void setData(List<GameData> data) {
        this.data = data;
    }

    public GameMeta getMeta() {
        return meta;
    }

    public void setMeta(GameMeta meta) {
        this.meta = meta;
    }

}


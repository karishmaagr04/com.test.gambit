package com.test.gambit.Game;
/*
 * @author Karishma Agrawal on 2019-05-24.
 */

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.gambit.Core.AbstractFragment;
import com.test.gambit.Core.ViewModelEvent;
import com.test.gambit.DataApi;
import com.test.gambit.Game.GameDataModel.GameData;
import com.test.gambit.PaginationScrollListener;
import com.test.gambit.R;
import com.test.gambit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GameFragment extends AbstractFragment<GameViewModel> {

    private RecyclerView recyclerView;
    private GameAdapter gameAdapter;
    private ProgressDialog progressDialog;
    private List<GameData> gameDataList = new ArrayList<>();
    private int i = 1;

    private int pageStart = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int total_count = 47526;
    private int per_page = 25;
    private int currentPage = 1;
    private DataApi service;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_game, container, false);
        return root;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view_game);
        service = RetrofitClientInstance.getCacheEnabledRetrofit(Objects.requireNonNull(getActivity())).create(DataApi.class);
        getGameList();
    }

    private void getGameList() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        setAdapter(gameDataList);

        /*Create handle for the RetrofitInstance interface*/
        while (pageStart <= per_page) {
            Log.d("first", String.valueOf(pageStart));
            viewModel.loadFirstDataCall(currentPage,service);
            currentPage++;
            pageStart++;
        }
    }

    private void setAdapter(List<GameData> gameData) {
        gameAdapter = new GameAdapter(gameData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(gameAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                pageStart = 1;
                while (pageStart <= per_page) {
                    viewModel.loadFirstDataCall(currentPage,service);
                    currentPage++;
                    pageStart++;
                }
            }

            @Override
            public int getCurrentPage() {
                return currentPage;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    protected void registerEvents() {
        registerEvent("abc");
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
    }

    @Nullable
    @Override
    protected Class getViewModel() {
        return GameViewModel.class;
    }

    @Override
    protected void onDataChanged(ViewModelEvent viewModelEvent) {
        super.onDataChanged(viewModelEvent);
        if (viewModelEvent.eventType.equals("abc")) {
            progressDialog.dismiss();
            Log.d("game_data_fragment", String.valueOf(i));
            setGameAdapter(viewModelEvent);
        }
    }

    private void setGameAdapter(ViewModelEvent viewModelEvent) {
        gameAdapter.add((GameData) viewModelEvent.eventData);
        if (currentPage < total_count) gameAdapter.addLoadingFooter();
        else {
            isLastPage = true;
            isLoading = false;
            gameAdapter.clear();
        }
    }
}

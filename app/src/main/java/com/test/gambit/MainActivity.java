package com.test.gambit;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.test.gambit.Game.GameFragment;
import com.test.gambit.Player.PlayerFragment;

public class MainActivity extends AppCompatActivity {
    private TextView label;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar2);
        label = findViewById(R.id.label);
        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_view);

        setSupportActionBar(toolbar);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(getString(R.string.activity_label));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        setAdapter();

    }

    private void setAdapter() {
        DataPagerAdapter dataPagerAdapter = new DataPagerAdapter(getSupportFragmentManager());
        dataPagerAdapter.addFragment(new PlayerFragment(), "Player");
        dataPagerAdapter.addFragment(new GameFragment(), "Game");
        viewPager.setAdapter(dataPagerAdapter);
        viewPager.measure(viewPager.getMeasuredWidth(),viewPager.getMeasuredHeight());
        tabLayout.setupWithViewPager(viewPager);
    }




}
